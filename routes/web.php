<?php

use App\Http\Controllers\user\ProductController;
use App\Http\Controllers\user\UserController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\user\UserController as UserUserController;
use GuzzleHttp\Middleware;
use Laravel\Sanctum\Sanctum;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('/home', function () {
    return view('welcome');
})->middleware('verified');






////// Start User ///////
Auth::routes( ['verify' =>true]);
Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']], function () {
Route::resource('/user', UserUserController::class);
});
/////// End User ///////


/////Start Product //////

Route::group(['prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']], function () {
    Route::resource('/product', ProductController::class);
    });
    Route::post('/product/update/{product_id}',[ProductController::class,'update'])->name('product.update');
    Route::get('/user-products/{user_id}', [ProductController::class,'userProduct']);
////// End Product //////



///////////// profile ////////
Route::get('/profile',[UserController::class,'profile'])->name('profile');
Route::get('/profile/edit/{user_id}',[UserController::class,'profileEdit'])->name('profile.edit');
Route::put('/profile/update/{user_id}',[UserController::class,'profileUpdate'])->name('profile.update');
Route::get('/profile/changepassword', [UserController::class,'changePasswordForm'])->name('profile.change.password');
Route::post('/profile/changepassword',[UserController::class,'changePassword'])->name('profile.changepassword');
