<?php

use App\Http\Controllers\api\AccesTokenController;
use Illuminate\Http\Request;
use Illuminate\Routing\RouteRegistrar;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\NewPasswordController;
use App\Http\Controllers\api\EmailVerificationController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
////// Start User //////
Route::middleware(['auth:sanctum'])->get('/user/auth', function (Request $request) {
    return $request->user();
});
Route::get('/test',function(){
    return 'prod';  // nothing
});
Route::apiResource('user',\App\Http\Controllers\Api\UserController::class);
Route::get('paginate-user',[\App\Http\Controllers\Api\UserController::class,'pagintion']);
Route::post('/register', [\App\Http\Controllers\Api\UserController::class,'store']);
Route::post('/login', [AccesTokenController::class,'store']);
Route::post('logout', [\App\Http\Controllers\Api\UserController::class, 'logout'])->middleware('auth:sanctum');

Route::post('forgot-password', [\App\Http\Controllers\Api\NewPasswordController::class, 'forgotPassword']);
Route::post('reset-password', [NewPasswordController::class,'reset']);

Route::post('email/verification-notification', [EmailVerificationController::class, 'sendVerificationEmail'])->middleware('auth:sanctum');
Route::get('verify-email/{id}/{hash}', [EmailVerificationController::class, 'verify'])->name('verification.verify')->middleware('auth:sanctum');
/////// End User ///////////


/////// Start Product ///////
Route::apiResource('product',\App\Http\Controllers\Api\ProductController::class);
Route::get('/user-products/{user_id}', [\App\Http\Controllers\Api\ProductController::class,'userProduct']);

//////// End Product ///////
