<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin=  User::create([
            'first_name'=>'admin',
            'email'=>'admin00@gmail.com',
            'password'=>Hash::make(123456),
            'phone_number'=>'093202921',
            'last_name'=>'admin',
        ]);
        $admin->attachRole('super_admin');
    }
}
