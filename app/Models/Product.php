<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
class Product extends Model
{
    use HasFactory;
    protected $fillable=['name','description','image'] ;
    protected $hidden=['created_at','updated_at','user_id'];
    protected $appends=[ 'image_url' ];
    public function getImageUrlAttribute(){
       if(!$this->image){
           return'data:image/jpeg;base64,/9j/';
       }
       if(Str::startsWith($this->image,['http://','http://'])){
           return $this->image;
       }
   return asset('images/products/' .$this->image);
   }
 public function users(){
    return $this ->belongsTo('App\Models\User','user_id','id');
}
}
