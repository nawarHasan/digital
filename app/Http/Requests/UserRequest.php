<?php

namespace App\Http\Requests;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            [
                'last_name'=>'required',
               'first_name'=>'required',
               'email'=>'required'|'unique',
               'password'=>'required',
               'phone_number'=>'required'|'unique',
        ]];

    }
}
