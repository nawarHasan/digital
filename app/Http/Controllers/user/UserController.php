<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email'=>'email|unique:App\Models\User,email',
           'first_name'=>'string',
           'last_name'=>'string',
           'password'=>'min:5|max:20',
           'phone_number'=>'required|unique:App\Models\User,phone_number'
        ]);
        $data = $request->all();
        $user = User::create($data);
        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('user.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user = User::findOrFail($id);

        return view('user.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user= User::findOrFail($id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->phone_number = $request->phone_number;
        $user->save();
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect()->route('user.index');
    }
    public function logout(Request $request)
    {
        Auth::guard()->logout();
        $request->session()->invalidate();

       $request->session()->regenerateToken();

       return redirect('/');
   }

   public function profile()
   {   $user=auth()->user();
       return view('profile.index',compact('user'));
   }

   public function profileEdit($id)
   {

       $user = User::findOrFail($id);
       return view('profile.edit',compact('user'));
   }

   public function profileUpdate(Request $request,$id)
   {
       $request->validate([
           'name'  => 'required|string|max:255',
           'email' => 'required|string|email|max:255|unique:users,email,'.auth()->id()
       ]);


       $user =  User::findOrFail($id);
       $user->name = $request->name;
       $user->email = $request->email;
       if ($request->hasFile('photo')) {
           $file_ext=$request->photo->getClientOriginalExtension() ;
                   $file_name=time().'.'.$file_ext;
                   $path='images/profile';
                   $request->photo->move($path,$file_name);
                   $user->profile_picture=$file_name;
               }


       //$user=auth()->user() tutorial
         // $user =new User();
       // $user=auth()->user();
       //  $user =new User;
       //  $user=DB::table('users')
       //  ->where([
       //     'name'              => auth()->user()->name,
       //     'email'             => auth()->user()->email,
       //     'profile_picture'   => $profile])
       //     ->update([
       //         'name'              => $request->name,
       //         'email'             => $request->email,
       //         'profile_picture'   => $profile]);
       // $user->save();
       $user->save();
       return redirect()->route('profile');
   }

   /**
    * CHANGE PASSWORD
    */
   public function changePasswordForm()
   {
       return view('profile.changepassword');
   }

   public function changePassword(Request $request)
   {
       if (!(Hash::check($request->get('currentpassword'), Auth::user()->password))) {
           return back()->with([
               'msg_currentpassword' => 'Your current password does not matches with the password you provided! Please try again.'
           ]);
       }
       if(strcmp($request->get('currentpassword'), $request->get('newpassword')) == 0){
           return back()->with([
               'msg_currentpassword' => 'New Password cannot be same as your current password! Please choose a different password.'
           ]);
       }

       $this->validate($request, [
           'currentpassword' => 'required',
           'newpassword'     => 'required|string|min:8|confirmed',
       ]);

       // $user=auth()->user();
       // $user=new User;
       $user=DB::table('users')
       ->where(['password'=>auth()->user()->password])

       ->update( ['password' => bcrypt($request->newpassword)]);

       // $user->password = bcrypt($request->get('newpassword'));
       // $user->save();

       Auth::logout();
       return redirect()->route('login');
   }
}
