<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
    {
        $this->middleware('role:admin')->except('userProduct');
    }
    public function index()
    {
        $products = Product::all();
        return view('product.index',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new Product;
        $product['name']        = $request->name;
        $product['description'] = $request->description;

        if ($request->hasFile('photo')) {
            $file_ext=$request->photo->getClientOriginalExtension() ;
                    $file_name=time().'.'.$file_ext;
                    $path='images/products';
                    $request->photo->move($path,$file_name);
                    $product->image=$file_name;

          }
        $product->save();
        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return view('product.show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);

        return view('product.edit',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $data['name']        = $request->name ? $request->name : $product->name;
        $data['description'] = $request->description ? $request->description : $product->description;
        if ($request->hasFile('photo')) {
            $file_ext=$request->photo->getClientOriginalExtension() ;
                    $file_name=time().'.'.$file_ext;
                    $path='images/products';
                    $request->photo->move($path,$file_name);
                    $product->image=$file_name;
        }
           // $doctor->image = $request->photo;
        $product->update($data);
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return redirect()->route('product.index');
    }
    public function userProduct($id)
    {
        $user=User::find($id);
          $user_products= Product::whereHas('users')->where('user_id',$id)->get();
             return view('user.user-products',compact('user_products','user'));
    }
}
