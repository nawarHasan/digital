<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $products = Product::paginate();
        return  response()->json($products,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    $product = new Product;
    $product['name']        = $request->name;
    $product['description'] = $request->description;

    if ($request->hasFile('photo')) {
        $file_ext=$request->photo->getClientOriginalExtension() ;
                $file_name=time().'.'.$file_ext;
                $path='images/products';
                $request->photo->move($path,$file_name);
                $product->image=$file_name;

      }
    $product->save();
    return response()->json([
        'status' => true,
        'message' => 'Product Created Successfully',
        'product' => $product,
    ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function userProduct($id)
    {

        $user_products= Product::whereHas('users')->where('user_id',$id)->get();
        return  response()->json($user_products,200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $data['name']        = $request->name ? $request->name : $product->name;
        $data['description'] = $request->description ? $request->description : $product->description;
        if ($request->hasFile('image')) {

            $oldimage = $product->image;
            $image = $request->file('image');
            $data['image'] = $this->images($image, $oldimage);
        }
        $product->update($data);
                return response()->json([
                    'status'=>true,
                    'data' => $product,
                    'message' => 'Request Information Updated Successfully',
                ]);
    }
    public function show($id)
    {
        $product= Product::findOrFail($id);
        return  response([
            'product'=>$product
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return response()->json([
            'status'=>true,
            'message' => 'Request Information deleted Successfully',
        ]);
    }
}
