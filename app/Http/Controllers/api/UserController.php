<?php

namespace App\Http\Controllers\api;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Access\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:sanctum')->only('destroy');
    }
    public function index()
    {

     $users = User::all();
      return  response([
       'users'=>$users
         ],200);
   }

    public function pagintion(){

      $users = User::paginate();
        return  response()->json($users,200);
}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

        protected function store(Request $request)
        {

            $user = new User;
            $user['first_name'] = $request->first_name;
            $user['last_name'] = $request->first_name;
            $user['phone_number'] = $request->phone_number;
            $user['email']        = $request->email;
            $user['password'] = Hash::make($request->password);

        $user->save();
        return response()->json([
            'status' => true,
            'message' => 'User Created Successfully',
            'user' => $user,
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user= User::findOrFail($id);
        return  response([
            'user'=>$user
        ],200);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user= User::findOrFail($id);
        $data['first_name'] = $request->first_name ? $request->first_name : $user->first_name;
        $data['last_name'] = $request->last_name ? $request->last_name : $user->lasr_name;
        $data['password'] = $request->password ? $request->password : $user->password;
        $data['phone_number'] = $request->phone_number ? $request->phone_number : $user->phone_number;
        $data['email'] = $request->email ? $request->email : $user->email;
        $user->update($data);
        return response()->json([
            'status'=>true,
            'data'=>$user,
            'message' => 'User Updated Successfully',
        ]);
}

public function logout(Request $request)
{

    $request->user()->tokens()->delete();

    return response()->json(
        [
            'message' => 'Logged out'
        ]
    );
}


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
    return response()->json([
        'status'=>true,
        'message' => 'Request Information deleted Successfully',
    ]);

    }
    public function verify(){
        return view('dashboard');
    }
}
