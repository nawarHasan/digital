<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
class AccesTokenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'email|max:255',
            'phone_number'=>'numeric',

        ]);

        if($user = User::where( 'email', $request->email )->first()){
            if ($user && Hash::check($request->password, $user->password)) {
            $device_name = $request->post('device_name', $request->userAgent());
            $token = $user->createToken($device_name);
            return \Illuminate\Support\Facades\Response::json([
                'token' => $token->plainTextToken,
                'user' => $user,
            ], 201);
        }}


        elseif($user = User::where( 'phone_number', $request->phone_number )->first()){
            if ( $user->password == $request->password) {
                $device_name = $request->post('device_name', $request->userAgent());
                $token = $user->createToken($device_name);
                return \Illuminate\Support\Facades\Response::json([
                    'token' => $token->plainTextToken,
                    'user' => $user,
                ], 201);
            }}

        return \Illuminate\Support\Facades\Response::json([
            'code' => 0,
            'message' => 'invaled credentioal'
        ], 401);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
