<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    // public function checkUserLogin(Request $request)
    // {
        // $this->validate($request, [
        //     // 'full_name'   => 'required|full_name',
        //     'password' => 'required|min:6'
        // ]);

        // if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password])) {

        //     return redirect()->intended('/lab');
        // }
        //  elseif (Auth::guard('web')->attempt(['ali' => $request->email, 'password' => $request->password])){
        //      return redirect()->intended('/lab');
        //  }
        public function username()
        {

            $value = request()->input('identify'); // nawar@gmail.com  or 0945458778
            $field = filter_var($value, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone_number';
            request()->merge([$field =>$value ]);
            return  $field;


    //     return back()->withInput($request->only('full_name'));
    // }
        }


    public function logout(Request $request,)
 {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

       $request->session()->regenerateToken();

        return redirect('/');
     }
}
