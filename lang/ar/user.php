<?php

return [
    'first_name' => 'الاسم الاول ',
    'last_name' => 'الاسم الاخير ',
    'phone_number' => 'رقم الهاتف',
    'email' => 'الايميل',
    'save_now'=>'حفظ الأن',
    'password'=>'كلمة السر',
    'name'=>'اسم',
    'create'=>'انشاء',
];
