<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<div class="page-title">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

<div class="page-title">
    <div class="row">
        <div class="col-sm-6">
            <h4 class="mb-0">USER-NAME: {{ $user->last_name }} {{ $user->last_name }}</h4>

        </div>
    </div>
</div>

<!-- row -->
<div class="row">
    <div class="col-md-12 mb-30">
        <div class="card card-statistics h-100">
            <div class="card-body">

                <table id="table_id" class="display" class="table">
                    {{-- <thead> --}}
                        <tr>
                         <th style="width: 50px">user-id</th>
                         <td>{{ $user->id }}</td>
                        </tr>
                        <tr>
                            <th style="width: 100px">first-name</th>
                            <td>{{ $user->first_name }}</td>
                        </tr>
                        <tr>
                            <th style="width: 140px">last-name</th>
                            <td>{{ $user->last_name }}</td>
                        </tr>

                    <tr>
                            <th style="width: 100px">phone-number</th>
                            <td>{{ $user->phone_number }}</td>
                        </tr>
                        <tr>
                            <th>email</th>
                            <td>{{ $user->email }}</td>
                        </tr>

        </tr>


                    </table>
                    <br>

                    <a class="btn btn-outline-primary btn-md  btn-lg" type="submit" href="/user" >Show all Users</a>
                </div>





