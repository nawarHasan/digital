
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<div class="page-title">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link
    href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css"
    rel="stylesheet"  type='text/css'>
    <div class="row">
        <div class="col-sm-6">
            <h4 class="mb-0">All products to : {{$user->first_name}} {{$user->last_name}}</h4>
        </div>

    </div>
</div>
<!-- breadcrumb -->

<!-- row -->
<div class="row">
    <div class="col-md-12 mb-30">
        <div class="card card-statistics h-100">
            <div class="card-body">

                <table id="table_id" class="display">
                    <thead>
                        <tr>

                            <th>name</th>
                            <th style="padding-left:10px ">description</th>
                            <th style="padding-left: 15px">image</th>




                            {{-- <th style="padding-left: 15px">User.Control</th> --}}





                            {{-- @endrole --}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($user_products as $user_product)
                        <tr>
                            {{-- <td>{{ $admin->id }}</td> --}}


                            <td style="padding-left:10px ">{{ $user_product->name }}</td>
                            <td style="padding-left:10px ">{{ $user_product->description }}</td>
                            <td style="height:190px " ><img style="height:170px " src="/images/products/{{$user_product->image}}"></td>

                            <td style="padding-left: 10px">
                                <a href="{{Route('user.show',$user_product->id)}}" class="btn btn-primary btn-sm">
                                    <i class="fa fa-eye"></i>
                                </a>
                                <a href="{{Route('user.edit',$user_product->id)}}" class="btn btn-warning btn-sm">
                                    <i class="fa fa-edit"></i>
                                </a>

                                <form action="{{Route('user.destroy',$user_product->id)}}" method="post" style="display:inline">
                                    @csrf
                                    @method('delete')

                                    <button type="submit" class="btn btn-danger btn-sm">
                                        <i class="fa fa-trash"></i>
                                 {{-- @endrole --}}
                                    </button>
                                </form>


                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

