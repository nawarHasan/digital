<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<div class="page-title">
    <div class="row">
        <div class="col-sm-6">
            <h4 class="mb-0" style="color: #556AFF;"> <b>{{__('user.create')}}</b> </h4>
        </div>

    </div>
</div>

<!-- breadcrumb -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    {{-- <a class="navbar-brand" href="#"></a> --}}
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                <li class="nav-item active">
                    <a class="nav-link"
                       href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}"> {{ $properties['native'] }}
                        <span class="sr-only"></span></a>
                </li>
            @endforeach


        </ul>
    </div>
</nav>
<!-- row -->
<div class="row">
    <div class="col-md-12 mb-30">
        <div class="card card-statistics h-100">
            <div class="card-body">



                <form method="post" enctype="multipart/form-data" action="{{Route('user.store')}}" autocomplete="off">

                    @csrf

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="first_name">{{__('user.first_name')}}<span class="text-danger">*</span></label>
                                <input  type="text" name="first_name"  class="form-control" placeholder="{{__('user.first_name')}}">
                                @error('first_name')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="last_name">  {{__('user.last_name')}}<span class="text-danger">*</span></label>
                                <input  class="form-control" name="last_name" type="text" placeholder={{__('user.last_name')}}>
                                @error('last_name')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="email">  {{__('user.email')}}<span class="text-danger">*</span></label>
                                <input  class="form-control" name="email" type="email" placeholder={{__('user.email')}} >
                                @error('email')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="password">  {{__('user.password')}}<span class="text-danger">*</span></label>
                                <input  class="form-control" name="password" type="text" placeholder={{__('user.password')}} >
                                @error('password')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="phone_number">  {{__('user.phone_number')}}<span class="text-danger">*</span></label>
                                <input  class="form-control" name="phone_number" type="text" placeholder={{__('user.phone_number')}} >
                                @error('phone_number')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>





                   </div>
<div style="padding-left:660px ">

                   {{-- <a class="btn btn-outline-primary btn-md  btn-lg" type="submit" href="/Event/event/create" >save and create another</a> --}}

                  <button type="submit" style="margin: 10px;" class="btn btn-primary btn-md  btn-lg" >{{__('user.save_now')}}</button>
                </div>
                </form>


            </div>
        </div>
    </div>
</div>
