<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<div class="page-title">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

<style>


#upload-photo {
   opacity: 0;
   position: absolute;
   z-index: -1;
}
.avatar-img{
    position: relative;
}
.avatar-label {
   cursor: pointer;
   position:absolute;
   top: 20px;
   right: 10px;
   font-size: 20px;
   border-radius:60%;
   border:solid black 1px;
   padding:5px;
   color: white;
    background-color: #673ab7;
    width: 25px;
    height: 25px;
    border-radius: 50%;
    line-height: 10px;
    /* font-size: 10px; */
    text-align: center;
    cursor: pointer;
    /* z-index: 999; */
   /* Style as you please, it will become the visible UI component. */
}
.avatar {
  vertical-align: middle;
  width: 120px;
  height: 120px;
  border-radius: 50%;
}
.add-sign{

}
</style>



<!-- breadcrumb -->
<div class="page-title">
    <div class="row">
        <div class="col-sm-6">
            <h4 class="mb-0 " >  Edit</h4>
        </div>

    </div>
</div>
<!-- breadcrumb -->

<!-- row -->
<div class="row">
    <div class="col-md-12 mb-30">
        <div class="card card-statistics h-100">
            <div class="card-body">



                <form method="post" enctype="multipart/form-data" action="{{Route('product.update',$product->id)}}" autocomplete="off">

                    @csrf

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="name">name<span class="text-danger">*</span></label>
                                <input  type="text" name="name" value="{{old('name',$product->name)}}" class="form-control">
                                @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="description"> description <span class="text-danger">*</span></label>
                                <input  class="form-control" name="description" value="{{old('description',$product->description)}}" type="text" >
                                @error('description')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="photo"> image<span class="text-danger">*</span></label>
                                <input  class="form-control" name="photo" type="file" accept="image/*">
                                @error('image')
                                <p class="alert alert-danger">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                    </div>








                   <button type="submit" style="margin: 10px;" class="btn btn-success btn-md  btn-lg" >Update</button>


                </form>


            </div>
        </div>
    </div>
</div>
<!-- row closed -->
